/**
 * Created by ud
 */

var db = {};
var async = require('async');
var __logger = require('../logger');

db.mongo = require('./mongo');



db.initialize = function (__callback) {
    async.series(
        [
            db.mongo.init,

        ],
        function (err, results) {
            if (err) {
                __logger.error('failed to run all databases', {err: err});
                __callback(err);
            } else {
                __callback(null);
            }
        }
    );
};


module.exports = db;
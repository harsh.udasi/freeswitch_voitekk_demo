var esl = require('modesl');
var db = require('../fs_assignment_2/lib/db');
var __logger = require('../fs_assignment_2/lib/logger');
var redis = require('redis');

var esl_server = new esl.Server({port: 8085, myevents:true}, function(){
    console.log("esl server is up");
});

db.initialize(function (err) {
    if (err) {
        __logger.error('error connecting mongo',err);
        process.exit(1);
    } else {
        console.log('started all databases');
        __logger.info('started all databases at: ' + new Date().getTime())
    }
});
var redis_client = redis.createClient();

redis_client.on('error',function(err){
    console.log('error connecting redis------- ', err);
    __logger.error('error connecting redis -------- ', err);
});

esl_server.on('connection::ready', function(conn, id) {
    console.log('new call ' + id);
    __logger.debug('call patched, id is: ',id);
    conn.call_start = new Date().getTime();
//    console.log('nnnnnnn' + JSON.stringify(conn.getInfo()));
//    console.log(',m,m' + conn.getInfo().getHeader('variable_effective_caller_id_number'));
    var uuid = conn.getInfo().getHeader('Channel-Call-UUID');
    var source = conn.getInfo().getHeader('Caller-Orig-Caller-ID-Number');
    var destination = conn.getInfo().getHeader('Caller-Destination-Number');
    console.log('call started at::: ' + conn.call_start);
    __logger.info('call started at::: ' + conn.call_start);
    conn.execute('bridge','{origination_caller_id_number=9999999}sofia/internal/1200%127.0.0.1');
//    conn.execute('answer');
    console.log('call answered');
    __logger.info('call successfully answered');
    conn.execute('echo', function(){
        console.log('echoing');
    });

    conn.on('esl::end', function(evt, body) {
        this.call_end = new Date().getTime();
        var delta = (this.call_end - this.call_start) / 1000;
        console.log("Call duration " + delta + " seconds");
        __logger.debug("Call duration " + delta + " seconds");
        var insert_dict = {
            'uuid':uuid,
            'source':source,
            'destination':destination,
            'start_epoch':conn.call_start,
            'end_epoch':this.call_end,
            'duration':delta
        };
        var collection = 'call_cdr';
        db.mongo.__insert(collection,insert_dict,function(err,result){
            if(err) {
                console.log("Error in cdr insertion in mongo", err);
                __logger.error("Error in cdr insertion in mongo", err);
            }
            else {
                //res.send(public_add);
                console.log("CDR successfully inserted in mongo", result);
                __logger.debug("CDR successfully inserted in mongo", result);
            }
        });
        redis_client.lpush(['fs_uuid_mem_cache',uuid],function(err,result){
            if(err){
                console.log('error in inserting uuid in redis',err);
                __logger.error('error in inserting uuid in redis',err)
            }
            else{
                console.log('inserted uuid successfully in redis',result);
                __logger.debug('inserted uuid successfully in redis',result);
            }
        })
    });
});